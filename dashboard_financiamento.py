#coding=utf-8
import os
import unittest
import desired_capabilities_pan
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver
from time import sleep
import threading
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import string
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException

SLEEPY_TIME = 0.1
package = 'com.br.tokenlab.bancopan:id/'
root = '//android.widget.ExpandableListView/android.widget.LinearLayout'

class loginTests(threading.Thread):
	def __init__(self, port, spec, tipo):
		threading.Thread.__init__(self)
		self.port = port
		self.spec = spec
		self.tipo = tipo

	def getContext(self):
		btn_login = self.driver.find_element_by_id(package + 'btn_login')
		login = self.driver.find_element_by_id(package + 'edt_email')
		senha = self.driver.find_element_by_id(package + 'edt_password')
		return btn_login, login, senha

	def run(self):
		#connecting to the server
		self.setup()

		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'btn_login')))
		btn_login, login, senha = self.getContext();

		#login successful
		self.loginSuccessful(btn_login, login, senha)

		financiamento = self.nonBlockedLoan()

		self.checkEye(financiamento)

		self.checkPad1()

		self.checkPad2()

		self.checkPad3()

		self.checkPad4()

	def setup(self):
		desiredCaps = desired_capabilities_pan.get_desired_capabilities('pan.apk')
		self.driver = webdriver.Remote('http://localhost:' + str(self.port) + '/wd/hub', desiredCaps)
		self.action = TouchAction(self.driver)
		sleep(SLEEPY_TIME)

	def loginSuccessful(self, btn_login, login, senha):
		#login successful 
		senha_txt = "102030"
		login.send_keys("pedro.fogolin@gmail.com")
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, package + 'group_item_product_selector_title')))
		
		label = self.driver.find_element_by_id(package + 'group_item_product_selector_title')
		
		print 'Teste login com sucesso... ' + self.spec 
		if(label.text == 'Cartão'.decode('utf8') or label.text == 'Cartões'.decode('utf8') or label.text == 'Financiamento'.decode('utf8') or label.text == 'Financiamentos'.decode('utf8')):
			print 'OK!'
		else:
			print 'Not OK!'

	def nonBlockedLoan(self):
		layouts = self.driver.find_elements_by_xpath(root)
		
		label_bloco = layouts[0].find_element_by_class_name('android.widget.TextView')
		if(label_bloco.text == 'Financiamento'.decode('utf8') or label_bloco.text == 'Financiamentos'.decode('utf8')):
			i = 2
			flag_existe = 0

			while(i <= len(layouts)):
				try:
					layout_linha = self.driver.find_element_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout')
					elementos_linha = self.driver.find_elements_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout/android.widget.TextView')
					
					if(len(elementos_linha) == 1):
						nome_financiamento = elementos_linha[0].text
						self.action.tap(elementos_linha[0]).perform()
						return nome_financiamento
				except NoSuchElementException as e:
					pass

				i += 1
		elif(label_bloco.text == 'Cartão'.decode('utf8') or label_bloco.text == 'Cartões'.decode('utf8')):
			i = 2
			while(i <= len(layouts)):
				try:
					layout_linha = self.driver.find_element_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout')
					elementos_linha = self.driver.find_elements_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout/android.widget.TextView')
				except NoSuchElementException as e:
					while(i <= len(layouts)):
						try:
							layout_linha = self.driver.find_element_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout')
							elementos_linha = self.driver.find_elements_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout/android.widget.TextView')

							if(len(elementos_linha) == 1):
								flag_existe = 1
								nome_financiamento = elementos_linha[0].text
								self.action.tap(elementos_linha[0]).perform()
								return nome_financiamento
								sleep(3)
								break
						except NoSuchElementException as e:
							pass		
						i += 1
				i += 1
		if(flag_existe == 0):
			print 'Não há financiamentos para testar...'

	def checkEye(self, financiamento):
		WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'action_filter')))
		olho = self.driver.find_element_by_id(package + 'action_filter')
		self.action.tap(olho).perform()
		
		print 'Teste do olho.. ' + self.spec
		try:
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'group_item_product_selector_title')))
			
			label = self.driver.find_element_by_id(package + 'group_item_product_selector_title')
			
			if(label.text == 'Cartão'.decode('utf8') or label.text == 'Cartões'.decode('utf8') or label.text == 'Financiamento'.decode('utf8') or label.text == 'Financiamentos'.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		except TimeoutException as e:
			print 'Not OK!'

		try:
			financiamento_label = self.driver.find_element_by_name(financiamento)
			self.action.tap(financiamento_label).perform()
		except:
			print 'Financiamento não encontrado...'

	def checkPad1(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, package + 'block_1_message_intro')))
		texto_bloco1_quitado = 'Parabéns! Você já quitou seu financiamento PAN'
		texto_bloco1_bloqueado = 'Seu contrato foi enviado para uma assessoria de cobrança'
		texto_bloco1_atrasado = 'O valor para pagamento das parcelas em atraso é'
		texto_bloco1_em_dia = 'Você vai quitar o seu '
		label = self.driver.find_element_by_id(package + 'block_1_message_intro')
		try:
			valor = self.driver.find_element_by_id(package + 'block_1_message_value')
		except NoSuchElementException as e:
			pass

		print 'Teste bloco 1...'
		if(self.tipo == 1):
			if(label.text == texto_bloco1_quitado.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.tipo == 2):
			if(label.text == texto_bloco1_bloqueado.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.tipo == 3):
			if(label.text == texto_bloco1_atrasado.decode('utf8') and valor.text == '2'):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.tipo == 4):
			if(label.text == texto_bloco1_em_dia.decode('utf8') and valor.text == '3'):
				print 'OK!'
			else:
				print 'Not OK!'

	def checkPad2(self):
		texto_bloco2_sem_faturas = 'Você não tem mais parcelas a vencer'
		texto_bloco2_fatura_fechada = 'O número de parcelas atrasadas é'
		texto_bloco2_fatura_aberta_inicio = 'A sua próxima parcela com vencimento em '
		texto_bloco2_fatura_aberta_fim = ' é de'

		WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'block_2_message_intro')))
		label = self.driver.find_element_by_id(package + 'block_2_message_intro')
		try:
			valor = self.driver.find_element_by_id(package + 'block_2_message_value')
		except NoSuchElementException as e:
			pass

		print 'Teste bloco 2... ' + self.spec
		if(self.tipo == 1):
			if(label.text == texto_bloco2_sem_faturas.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.tipo == 2):
			if(label.text == texto_bloco2_fatura_aberta_inicio.decode('utf8') + '03/03' + texto_bloco2_fatura_aberta_fim.decode('utf8') and valor.text == 'R$ 9.999,99'):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.tipo == 3):
			if(label.text == texto_bloco2_fatura_fechada.decode('utf8') and valor.text == '2'):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.tipo == 4):
			if(label.text == texto_bloco2_fatura_aberta_inicio.decode('utf8') + '07/08' + texto_bloco2_fatura_aberta_fim.decode('utf8') and valor.text == 'R$ 1234'):
				print 'OK!'
			else:
				print 'Not OK!'

	def checkPad3(self):
		texto_bloco3_sem_pagamento = 'Você ainda não realizou nenhum pagamento'
		texto_bloco3_valido = 'O último pagamento realizado foi no dia'

		label = self.driver.find_element_by_id(package + 'block_3_message_intro')
		try:
			valor = self.driver.find_element_by_id(package + 'block_3_message_value')
		except NoSuchElementException as e:
			pass

		print 'Teste bloco 3... ' + self.spec
		if(self.tipo == 1):
			if(label.text == texto_bloco3_sem_pagamento.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		else:
			if(label.text == texto_bloco3_valido.decode('utf8') and valor.text == '09/08'):
				print 'OK!'
			else:
				print 'Not OK!'

	def checkPad4(self):
		texto_bloco4_pagas = 'O número de parcelas pagas é'

		label = self.driver.find_element_by_id(package + 'block_4_message_intro')
		try:
			valor = self.driver.find_element_by_id(package + 'block_4_message_value')
		except NoSuchElementException as e:
			pass

		print 'Teste bloco 4... ' + self.spec
		if(self.tipo == 1):
			if(label.text == texto_bloco4_pagas.decode('utf8') and valor.text == '0'):
				print 'OK!'
			else:
				print 'Not OK!'
		else:
			if(label.text == texto_bloco4_pagas.decode('utf8') and valor.text == '1'):
				print 'OK!'
			else:
				print 'Not OK!'

if __name__ == '__main__':
	tipo = 4
	thread1 = loginTests(4723, 'Nexus 6, Android 7.0', tipo)
	thread2 = loginTests(4724, 'Nexus 4, Android 6.0', tipo)

	thread1.start()
	# thread2.start()