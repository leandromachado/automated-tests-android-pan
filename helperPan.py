#coding=utf-8
from helper import *
from colorsprint import *

#definitions
SLEEPY_TIME = 0.1
package = 'com.br.tokenlab.bancopan:id/'
root = '//android.widget.ExpandableListView/android.widget.LinearLayout'

class helperPan:
	def __init__(self):
		pass

	def __call__(self):
		pass

	def initPan(self, id, port, file, product):
		helperTestsObj = helperTests()
		helperTestsObj.port = port
		helperTestsObj.file = file
		driver, action = helperTestsObj.setupEnvironment()
		
		self.driver = driver
		self.action = action

		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'btn_login')))
		btn_login, login, senha = self.getContext();

		self.loginSuccessful(btn_login, login, senha)
		if(product == 'cartao'):
			self.nonBlockedCard(id)

	def getContext(self):
		btn_login = self.driver.find_element_by_id(package + 'btn_login')
		login = self.driver.find_element_by_id(package + 'edt_email')
		senha = self.driver.find_element_by_id(package + 'edt_password')
		return btn_login, login, senha

	def nonBlockedCard(self, id):
		try:
			numero_cartao = 'Final ' + id
			cartao = self.driver.find_element_by_android_uiautomator('text(\"' + numero_cartao + '\")')
			self.action.tap(cartao).perform()
		except NoSuchElementException as e:
			print e

	def loginSuccessful(self, btn_login, login, senha):
		#login successful 
		senha_txt = "102030"
		login.send_keys("pedro.fogolin@gmail.com")
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, package + 'group_item_product_selector_title')))
		
		label = self.driver.find_element_by_id(package + 'group_item_product_selector_title')
		
		print 'Login... ' 
		if(label.text == 'Cartão'.decode('utf8') or label.text == 'Cartões'.decode('utf8') or label.text == 'Financiamento'.decode('utf8') or label.text == 'Financiamentos'.decode('utf8')):
			print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
		else:
			print bcolors.FAIL + 'Not OK!' + bcolors.ENDC

	def dismissBlockedCardAlert(self):
		print 'Blocked card...'
		try:
			alert = self.driver.find_element_by_android_uiautomator('text(\"' + 'Alerta' + '\")')
			cardBlocked = self.driver.find_element_by_android_uiautomator('text(\"' + 'Cartão bloqueado.' + '\")')
			continueButton = self.driver.find_element_by_android_uiautomator('text(\"' + 'Continuar' + '\")')
			self.action.tap(continueButton).perform()
			print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
		except:
			print bcolors.FAIL + 'Not OK!' + bcolors.ENDC

	def dismissErrorMessage(self):
		print 'Code error from backend...'
		try:
			alert = self.driver.find_element_by_android_uiautomator('text(\"' + 'Alerta' + '\")')
			continueButton = self.driver.find_element_by_android_uiautomator('text(\"' + 'Continuar' + '\")')
			self.action.tap(continueButton).perform()
			print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
		except:
			print bcolors.FAIL + 'Not OK!' + bcolors.ENDC

	def implicitWait(self, element):
		helperTestsObj = helperTests()
		helperTestsObj.implicitWait(self.driver, package + element)

	def dismissDisabledCardAlert(self):
		print 'Disabled card...'
		disabledCardMessage = 'Seu cartão está bloqueado. Por favor, entre em contato com a central de atendimento para obter mais informações.'
		alert = self.driver.find_element_by_android_uiautomator('text(\"' + 'Alerta' + '\")')
		disabledText = self.driver.find_element_by_android_uiautomator('text(\"' + disabledCardMessage.decode("utf8") + '\")')
		continueButton = self.driver.find_element_by_android_uiautomator('text(\"' + 'Continuar' + '\")')
		self.action.tap(continueButton).perform()

	def checkShowCodeButton(self, buttonShowCode):
		print '  Show payment code...'
		self.action.tap(buttonShowCode).perform()

		paymentCodeText = 'Código para pagamento'
		buttonCopyCodeText = 'Copiar Código'
		buttonSendCodeEmailText = 'Enviar Código por E-mail'

		paymentCode = self.driver.find_element_by_android_uiautomator('text(\"' + paymentCodeText.decode('utf8') + '\")')
		buttonCopyCode = self.driver.find_element_by_android_uiautomator('text(\"' + buttonCopyCodeText.decode('utf8') + '\")')
		buttonSendCodeEmail = self.driver.find_element_by_android_uiautomator('text(\"' + buttonSendCodeEmailText.decode('utf8') + '\")')

		print '  Copy code button...'
		try:
			codeCopiedText = 'Código copiado'
			self.action.tap(buttonCopyCode).perform()
			self.implicitWait('snackbar_text')

			codeCopied = self.driver.find_element_by_android_uiautomator('text(\"' + codeCopiedText.decode('utf8') + '\")')
			print bcolors.OKGREEN + '  OK!' + bcolors.ENDC
		except:
			print bcolors.FAIL + '  Not OK!' + bcolors.ENDC