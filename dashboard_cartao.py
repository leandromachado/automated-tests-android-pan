#coding=utf-8
#files
from helperPan import *

class cardDashboardTests(threading.Thread):
	def __init__(self, id, port, driver, action):
		threading.Thread.__init__(self)
		self.port = port
		self.action = action
		self.driver = driver
		self.numero_cartao = 'Final ' + id

	def run(self):
		if(self.numero_cartao == 'Final 4402'):
			#in this case, we should click in the alert
			helperPan.dismissBlockedCardAlert()

		#in this case, the web service returns an error code
		if(self.numero_cartao != 'Final 8851' and self.numero_cartao != 'Final 8853'):
			#check if the filter is working
			self.checkFilter()
			self.checkPad1()
			self.checkPad2()
			self.checkPad3()
			self.checkPad4()

			# self.servicesFlow()
		elif(self.numero_cartao == 'Final 8851'):
			helperPan.dismissErrorMessage()
		elif(self.numero_cartao != 'Final 8853'):
			helperPan.dismissDisabledCardAlert()

	def checkFilter(self):
		WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'action_filter')))
		filterIcon = self.driver.find_element_by_id(package + 'action_filter')
		self.action.tap(filterIcon).perform()
		
		print 'Filter test.. '
		try:
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'group_item_product_selector_title')))
			
			label = self.driver.find_element_by_id(package + 'group_item_product_selector_title')
			
			if(label.text == 'Cartão'.decode('utf8') or label.text == 'Cartões'.decode('utf8') or label.text == 'Financiamento'.decode('utf8') or label.text == 'Financiamentos'.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'

			cartao = self.driver.find_element_by_android_uiautomator('text(\"' + str(self.numero_cartao).decode("utf8") + '\")')
			self.action.tap(cartao).perform()
			sleep(1)
			if(self.numero_cartao == 'Final 4402'):
				helperPan.dismissBlockedCardAlert()
		except TimeoutException as e:
			print 'Not OK! - Timeout'
		except NoSuchElementException as e:
			print 'Card not found...'

	def checkPad1(self):
		WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, package + 'block_1_message_intro')))
		texto_bloco1 = 'O seu limite disponível para compras é de'
		label = self.driver.find_element_by_id(package + 'block_1_message_intro')
		valor = self.driver.find_element_by_id(package + 'block_1_message_value')

		print 'Pad 1... '
		if(self.numero_cartao == 'Final 4402'):
			if(valor.text == '' and label.text == texto_bloco1.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.numero_cartao == 'Final 8849'):
			if(valor.text == 'R$ 1000,00' and label.text == texto_bloco1.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.numero_cartao == 'Final 8850'):
			if(valor.text == 'R$ 5000,00' and label.text == texto_bloco1.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.numero_cartao == 'Final 8852'):
			if(valor.text == 'R$ 1.500,00' and label.text == texto_bloco1.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'

		self.action.tap(label).perform()
		try:
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'available_credit_limit_value')))
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'available_credit_limit_title')))
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'total_credit_limit_title')))
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'total_credit_limit_value')))
			
		except NoSuchElementException as e:
			print 'Algo faltando na tela de meu limite de crédito...'
			print e

		self.driver.back()

	def checkPad2(self):
		texto_bloco2_sem_faturas = 'Você não possui faturas para este cartão'
		texto_bloco2_fatura_fechada_inicio = 'O total de sua fatura com vencimento em '
		texto_bloco2_fatura_fechada_fim = ' é de'
		texto_bloco2_fatura_aberta_inicio = 'O total parcial de sua fatura com vencimento em '
		texto_bloco2_fatura_aberta_fim = ' é de'

		WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'block_2_message_intro')))
		label = self.driver.find_element_by_id(package + 'block_2_message_intro')

		print 'Pad 2... '
		if(self.numero_cartao == 'Final 4402' or self.numero_cartao == 'Final 8849'):
			if(label.text == texto_bloco2_sem_faturas.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		else:
			valor = self.driver.find_element_by_id(package + 'block_2_message_value')
			if(self.numero_cartao == 'Final 8850'):
				if(label.text == texto_bloco2_fatura_fechada_inicio.decode('utf8') + '08/08' + texto_bloco2_fatura_fechada_fim.decode('utf8') and valor.text == 'R$ 1.678,23'):
					print 'OK!'
				else:
					print 'Not OK!'
			elif(self.numero_cartao == 'Final 8851'):
				if(label.text == texto_bloco2_fatura_fechada_inicio.decode('utf8') + '09/09' + texto_bloco2_fatura_fechada_fim.decode('utf8') and valor.text == 'R$ 2.569,78'):
					print 'OK!'
				else:
					print 'Not OK!'
			elif(self.numero_cartao == 'Final 8852'):
				if(label.text == texto_bloco2_fatura_aberta_inicio.decode('utf8') + '02/07' + texto_bloco2_fatura_aberta_fim.decode('utf8') and valor.text == 'R$ 1.000,00'):
					print 'OK!'
				else:
					print 'Not OK!'

	def checkPad3(self):
		texto_bloco3 = 'Sua melhor data de compra é'

		label = self.driver.find_element_by_id(package + 'block_3_message_intro')
		valor = self.driver.find_element_by_id(package + 'block_3_message_value')

		print 'Pad 3... '
		if(self.numero_cartao == 'Final 8849'):
			if(label.text == texto_bloco3.decode('utf8') and valor.text == '07/07'):
				print 'OK!'
			else:
				print 'Not OK!'
		else:
			if(label.text == texto_bloco3.decode('utf8') and valor.text == '05/05'):
				print 'OK!'
			else:
				print 'Not OK!'

	def checkPad4(self):
		texto_bloco4_pontuacao = 'A sua pontuação atual no PAN MAIS é de'
		texto_bloco4_indisponivel = 'Pontuação indisponível. Tente novamente em alguns minutos'
		texto_bloco4_sem_beneficio = 'Você não possui nenhum programa de benefício'

		label = self.driver.find_element_by_id(package + 'block_4_message_intro')

		print 'Pad 4... '
		if(self.numero_cartao == 'Final 4402' or self.numero_cartao == 'Final 8849'):
			if(label.text == texto_bloco4_sem_beneficio.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		elif(self.numero_cartao == 'Final 8850' or self.numero_cartao == 'Final 8851'):
			if(label.text == texto_bloco4_indisponivel.decode('utf8')):
				print 'OK!'
			else:
				print 'Not OK!'
		else:
			valor = self.driver.find_element_by_id(package + 'block_4_message_value')
			if(self.numero_cartao == 'Final 8852'):
				if(label.text == texto_bloco4_pontuacao.decode('utf8') and valor.text == '1500'):
					print 'OK!'
				else:
					print 'Not OK!'
			elif(self.numero_cartao == 'Final 8853'):
				if(label.text == texto_bloco4_pontuacao.decode('utf8') and valor.text == '13.546'):
					print 'OK!'
				else:
					print 'Not OK!'

if __name__ == '__main__':
	cartoes = ['4402', '8849', '8850', '8851', '8852', '8853']
	port = 4723
	file = 'pan.apk'
	product = 'cartao'
	helperPan = helperPan()

	for id in cartoes:
			print 'Cartão Final ' + id
			helperPan.initPan(id, port, file, product)
			thread1 = cardDashboardTests(id, port, helperPan.driver, helperPan.action)

			thread1.start()
			while thread1.isAlive():
				pass