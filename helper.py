#coding=utf-8
import os
import unittest
import sys
from time import sleep
import threading
import string
import desired_capabilities_pan
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException

class helperTests:
	def __init__(self):
		pass

	def setupEnvironment(self):
		desiredCaps = desired_capabilities_pan.get_desired_capabilities(self.file)
		driver = webdriver.Remote('http://localhost:' + str(self.port) + '/wd/hub', desiredCaps)
		action = TouchAction(driver)
		return driver, action

	def implicitWait(self, driver, element):
		WebDriverWait(driver, 10).until(
				EC.presence_of_element_located(
					(By.ID, element)
				)
			)