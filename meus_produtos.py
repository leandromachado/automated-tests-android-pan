#coding=utf-8
import os
import unittest
import desired_capabilities_pan
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver
from time import sleep
import threading
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import string

SLEEPY_TIME = 0.1
package = 'com.br.tokenlab.bancopan:id/'
root = '//android.widget.ExpandableListView/android.widget.LinearLayout'

class loginTests(threading.Thread):
	def __init__(self, port, spec):
		threading.Thread.__init__(self)
		self.port = port
		self.spec = spec

	def getContext(self):
		btn_login = self.driver.find_element_by_id(package + 'btn_login')
		login = self.driver.find_element_by_id(package + 'edt_email')
		senha = self.driver.find_element_by_id(package + 'edt_password')
		return btn_login, login, senha

	def run(self):
		#connecting to the server
		self.setup()

		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'btn_login')))
		btn_login, login, senha = self.getContext();

		#login successful
		self.loginSuccessful(btn_login, login, senha)
		
		#scan scren
		self.checkBlockedCardAndPaidLoan() #ainda faltam as mensagens

	def setup(self):
		desiredCaps = desired_capabilities_pan.get_desired_capabilities('pan.apk')
		self.driver = webdriver.Remote('http://localhost:' + str(self.port) + '/wd/hub', desiredCaps)
		self.action = TouchAction(self.driver)
		sleep(SLEEPY_TIME)

	def loginSuccessful(self, btn_login, login, senha):
		#login successful 
		senha_txt = "102030"
		login.send_keys("pedro.fogolin@gmail.com")
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'group_item_product_selector_title')))
		
		label = self.driver.find_element_by_id(package + 'group_item_product_selector_title')
		
		print 'Teste login com sucesso... ' + self.spec 
		if(label.text == 'Cartão'.decode('utf8') or label.text == 'Cartões'.decode('utf8') or label.text == 'Financiamento'.decode('utf8') or label.text == 'Financiamentos'.decode('utf8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

	def checkBlockedCardAndPaidLoan(self):
		layouts = self.driver.find_elements_by_xpath(root)
		numero_cartoes = 0
		numero_financiamentos = 0
		indice_financiamento = 0
		
		label_bloco = layouts[0].find_element_by_class_name('android.widget.TextView')
		if(label_bloco.text == 'Cartão'.decode('utf8') or label_bloco.text == 'Cartões'.decode('utf8')):
			#agora iterar ate achar um com financiamento ou financiamento
			flag = 0
			i = 2

			while(i <= len(layouts)):
				try:
					layout_linha = self.driver.find_element_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout')
					elementos_linha = self.driver.find_elements_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout/android.widget.TextView')
					
					if(flag == 0):
						numero_cartoes += 1
					else:
						numero_financiamentos += 1
					if(len(elementos_linha) > 1):
						if(flag == 0):
							print 'Teste de ícone e mensagem de cartão bloqueado...' + self.spec
							try:
								if(elementos_linha[1].text == 'Bloqueado'):
									#adicionar parte do clique posteriormente
									print 'OK!'
							except NoSuchElementException as e:
								print 'Not OK!'
						elif(flag == 1):
							print 'Teste de ícone de financiamento liquidado...' + self.spec
							try:
								if(elementos_linha[1].text == 'Liquidado'):
									#adicionar parte do clique posteriormente
									print 'OK!'
							except NoSuchElementException as e:
								print 'Not OK!'

				except NoSuchElementException as e:
					texto_linha = self.driver.find_element_by_xpath(root + '[' + str(i) +']/android.widget.TextView')
					if(texto_linha.text == 'Financiamento'.decode('utf8') or texto_linha.text == 'Financiamentos'.decode('utf8')):
						flag = 1
						indice_financiamento = i
						print 'Teste cartão ou cartões...' + self.spec
						if(numero_cartoes > 1):
							if(label_bloco.text == 'Cartões'.decode('utf8')):
								print 'OK!'
							else:
								print 'Not OK!'
						else:
							if(label_bloco.text == 'Cartão'.decode('utf8')):
								print 'OK!'
							else:
								print 'Not OK!'
				i += 1

			label_bloco = layouts[indice_financiamento-1].find_element_by_class_name('android.widget.TextView')
			print 'Teste financiamento ou financiamentos...' + self.spec
			if(numero_cartoes > 1):
				if(label_bloco.text == 'Financiamentos'.decode('utf8')):
					print 'OK!'
				else:
					print 'Not OK!'
			else:
				if(label_bloco.text == 'Financiamento'.decode('utf8')):
					print 'OK!'
				else:
					print 'Not OK!'
		elif(label_bloco.text == 'Financiamento'.decode('utf8') or label_bloco.text == 'Financiamentos'.decode('utf8')):
			numero_cartoes = 0

			while(i <= len(layouts)):
				try:
					layout_linha = self.driver.find_element_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout')
					elementos_linha = self.driver.find_elements_by_xpath(root + '[' + str(i) + ']/android.widget.LinearLayout/android.widget.TextView')
					
					numero_financiamentos += 1

					if(len(elementos_linha) > 1):
						print 'Teste de ícone de financiamento liquidado...' + self.spec
						try:
							if(elementos_linha[1].text == 'Liquidado'):
								#adicionar parte do clique posteriormente
								print 'OK!'
						except NoSuchElementException as e:
							print 'Not OK!'

				except NoSuchElementException as e:
					print e
				i += 1

			label_bloco = layouts[0].find_element_by_class_name('android.widget.TextView')
			print 'Teste financiamento ou financiamentos...' + self.spec
			if(numero_financiamentos > 1):
				if(label_bloco.text == 'Financiamentos'.decode('utf8')):
					print 'OK!'
				else:
					print 'Not OK!'
			else:
				if(label_bloco.text == 'Financiamento'.decode('utf8')):
					print 'OK!'
				else:
					print 'Not OK!'

if __name__ == '__main__':
	thread1 = loginTests(4723, 'Nexus 6, Android 7.0')
	thread2 = loginTests(4724, 'Nexus 4, Android 6.0')

	thread1.start()
	# thread2.start()