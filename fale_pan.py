#coding=utf-8
import os
import unittest
import desired_capabilities_pan
from appium.webdriver.common.touch_action import TouchAction
from appium import webdriver
from time import sleep
import threading
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import string
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException

SLEEPY_TIME = 0.1
package = 'com.br.tokenlab.bancopan:id/'
root = '//android.widget.ExpandableListView/android.widget.LinearLayout'

class loginTests(threading.Thread):
	def __init__(self, port, spec):
		threading.Thread.__init__(self)
		self.port = port
		self.spec = spec

	def getContext(self):
		btn_login = self.driver.find_element_by_id(package + 'btn_login')
		login = self.driver.find_element_by_id(package + 'edt_email')
		senha = self.driver.find_element_by_id(package + 'edt_password')
		return btn_login, login, senha

	def run(self):
		#connecting to the server
		self.setup()
		# sleep(5)
		# self.driver.press_keycode(4)

		#para tirar o teclado
		fale_pan = self.driver.find_element_by_id(package + 'lnk_talk_to_pan')
		self.action.tap(fale_pan).perform()

		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'toolbar_title')))

		fale_pan = self.driver.find_element_by_id(package + 'toolbar_title')

		if(fale_pan.text == 'Fale com o PAN'):
			try:
				cac = self.driver.find_element_by_name('CAC - Central de Atendimento ao Cliente')
				self.financeira()
				self.cartoesECreditoConsignado()
				self.panSuaCasa()
				self.consorcio()
				self.seguros()
				corretora = self.driver.find_element_by_name('Corretora de Seguros')
				self.corretoraDeSeguros()
				sac = self.driver.find_element_by_name('SAC - Serviço de Atendimento ao Cliente')
				self.SACGeral()
				ouvidoria = self.driver.find_element_by_name('Ouvidoria')
				self.ouvidoria()
			except:
				'Problema na tela Fale com o PAN...'
	
	def setup(self):
		desiredCaps = desired_capabilities_pan.get_desired_capabilities('pan.apk')
		self.driver = webdriver.Remote('http://localhost:' + str(self.port) + '/wd/hub', desiredCaps)
		self.action = TouchAction(self.driver)
		sleep(SLEEPY_TIME)
		
	def financeira(self):
		print 'Teste Financeira...'
		try:
			financeira = self.driver.find_element_by_name('Financeira'.decode('utf8'))
			self.action.tap(financeira).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			#aqui é outra label
			financeira = self.driver.find_element_by_name('Financeira'.decode('utf8'))
			capitais = self.driver.find_element_by_name('Capitais e regiões metropolitanas'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('4002-1687'.decode('utf8'))
			horario1 = self.driver.find_element_by_name('2ª a  6ª, das 8h às 21h e sábados, das 9h às 15h'.decode('utf8'))
			demais_localidades = self.driver.find_element_by_name('Demais localidades'.decode('utf8'))
			tel2 = self.driver.find_element_by_name('0800-775-8686'.decode('utf8'))
			horario2 = self.driver.find_element_by_name('2ª a  6ª, das 8h às 21h e sábados, das 9h às 15h'.decode('utf8'))
			print 'OK!'
		except NoSuchElementException as e:
			print 'Not OK!'
			print 'Algum problema no link Financeira'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))

	def cartoesECreditoConsignado(self):
		print 'Teste Cartões e Crédito Consignado...'
		try:
			cartoes = self.driver.find_element_by_name('Cartões e Cartão Consignado'.decode('utf8'))
			self.action.tap(cartoes).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			#aqui é outra label
			cartoes = self.driver.find_element_by_name('Cartões e Cartão Consignado'.decode('utf8'))
			capitais = self.driver.find_element_by_name('Capitais e regiões metropolitanas'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('4003-0101'.decode('utf8'))
			horario1 = self.driver.find_element_by_name('De 2ª a sábado das 8h às 22h, inclusive aos feriados (exceto para Perda e Roubo e Aviso Viagem: 2ª a domingo - 24 horas)'.decode('utf8'))
			demais_localidades = self.driver.find_element_by_name('Demais localidades'.decode('utf8'))
			tel2 = self.driver.find_element_by_name('0800-888-0101'.decode('utf8'))
			horario2 = self.driver.find_element_by_name('De 2ª a sábado das 8h às 22h, inclusive aos feriados (exceto para Perda e Roubo e Aviso Viagem: 2ª a domingo - 24 horas)'.decode('utf8'))
			deficiente = self.driver.find_element_by_name('Atendimento a deficientes auditivos e de fala'.decode('utf8'))
			tel3 = self.driver.find_element_by_name('0800-776-2200'.decode('utf8'))
			horario3 = self.driver.find_element_by_name('2ª a domingo, 24 horas'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link Cartões e Crédito Consignado'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))

	def panSuaCasa(self):
		print 'Teste PAN Sua Casa...'
		try:
			pan_sua_casa = self.driver.find_element_by_name('PAN Sua Casa'.decode('utf8'))
			self.action.tap(pan_sua_casa).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			#aqui é outra label
			pan_sua_casa = self.driver.find_element_by_name('PAN Sua Casa'.decode('utf8'))
			cartoes = self.driver.find_element_by_name('Informações gerais relacionadas aos produtos da PAN Sua Casa'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('0800-600-3090'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link PAN Sua Casa'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))
	
	def consorcio(self):
		print 'Teste Consórcio...'
		try:
			consorcio = self.driver.find_element_by_name('Consórcio'.decode('utf8'))
			self.action.tap(consorcio).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			
			consorcio = self.driver.find_element_by_name('Consórcio'.decode('utf8'))
			consulta = self.driver.find_element_by_name('Consulta de contratos, saldos e demais serviços de Consórcios'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('0800-775-9393'.decode('utf8'))
			horario1 = self.driver.find_element_by_name('2ª a 6ª, das 8h às 21h e sábados, das 9h às 15h'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link Consórcio'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))

	def seguros(self):
		print 'Teste Seguros...'
		try:
			seguros = self.driver.find_element_by_name('Seguros'.decode('utf8'))
			self.action.tap(seguros).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			
			seguros = self.driver.find_element_by_name('Seguros'.decode('utf8'))
			consulta2 = self.driver.find_element_by_name('Consultas ou informações sobre apólices ou sinistros'.decode('utf8'))
			tel2 = self.driver.find_element_by_name('0800-775-9191'.decode('utf8'))
			horario2 = self.driver.find_element_by_name('2ª a 6ª, das 8h às 21h e sábados, das 9h às 15h'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link seguros'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))

	def corretoraDeSeguros(self):
		print 'Teste Corretora de Seguros...'
		try:
			seletores = self.driver.find_elements_by_id(package + 'subcategory_title')
			self.action.tap(seletores[5]).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			
			corretora = self.driver.find_element_by_name('Geral'.decode('utf8'))
			capitais = self.driver.find_element_by_name('Informações gerais da Corretora'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('(11) 3264-5160 ou corretora@grupopan.com'.decode('utf8'))
			horario1 = self.driver.find_element_by_name('2ª a 6ª, das 8h30 às 18h'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link Corretora de Seguros'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))

	def SACGeral(self):
		print 'Teste SAC...'
		try:
			seletores = self.driver.find_elements_by_id(package + 'subcategory_title')
			self.action.tap(seletores[6]).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))
			
			sac_geral = self.driver.find_element_by_name('Geral'.decode('utf8'))
			reclamacoes = self.driver.find_element_by_name('Reclamações, cancelamentos, sugestões, elogios e informações sobre produtos ou serviços'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('0800-776-8000'.decode('utf8'))
			horario1 = self.driver.find_element_by_name('Diariamente, 24 horas'.decode('utf8'))
			deficiente = self.driver.find_element_by_name('Atendimento a deficientes auditivo e de fala'.decode('utf8'))
			tel2 = self.driver.find_element_by_name('0800-776-2200'.decode('utf8'))
			horario2 = self.driver.find_element_by_name('Diariamente, 24 horas'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link SAC'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))
	
	def ouvidoria(self):
		print 'Teste Ouvidoria...'
		try:
			seletores = self.driver.find_elements_by_id(package + 'subcategory_title')
			self.action.tap(seletores[7]).perform()
			WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.ID, package + 'telephone')))

			ouvidoria = self.driver.find_element_by_name('Geral'.decode('utf8'))
			capitais = self.driver.find_element_by_name('Contate a Ouvidoria, caso não esteja satisfeito com a solução apresentada pelos demais canais de atendimento'.decode('utf8'))
			tel1 = self.driver.find_element_by_name('0800-776-9595'.decode('utf8'))
			horario1 = self.driver.find_element_by_name('2ª a 6ª, das 9h às 18h'.decode('utf8'))
			print 'OK!'
		except:
			print 'Not OK!'
			print 'Algum problema no link Ouvidoria'

		self.driver.press_keycode(4)
		WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.NAME, 'Fale com o PAN')))
	
if __name__ == '__main__':
	thread1 = loginTests(4723, 'Nexus 6, Android 7.0')
	thread2 = loginTests(4724, 'Nexus 4, Android 6.0')

	# thread1.start()
	thread2.start()