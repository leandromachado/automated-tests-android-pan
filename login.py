#coding=utf-8
import os
import unittest
import desired_capabilities_pan
from appium import webdriver
from appium.webdriver.common.touch_action import TouchAction
from time import sleep
import threading
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
import string

SLEEPY_TIME = 0.1
package = 'com.br.tokenlab.bancopan:id/'


class loginTests(threading.Thread):
	def __init__(self, port, spec):
		threading.Thread.__init__(self)
		self.port = port
		self.spec = spec

	def getContext(self):
		btn_login = self.driver.find_element_by_id(package + 'btn_login')
		login = self.driver.find_element_by_id(package + 'edt_email')
		senha = self.driver.find_element_by_id(package + 'edt_password')
		return btn_login, login, senha

	def run(self):
		#connecting to the server
		self.setup()

		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'btn_login')))
		btn_login, login, senha = self.getContext()

		# #login with no args
		self.loginIncompleteArgs(btn_login, login, senha)

		# #login with wrong args
		self.loginWrongArgs(btn_login, login, senha)

		# #login 3 times wrong
		# self.loginThreeTimesWrong(btn_login, login, senha)

		# #user not signed up
		self.loginNotSignedUp(btn_login, login, senha)

		# #user signed up with no products
		self.loginNoProducts(btn_login, login, senha)

		# #no connectivity
		self.driver.mobile.set_network_connection(self.driver.mobile.AIRPLANE_MODE)
		sleep(1)
		btn_login, login, senha = self.getContext();
		self.loginNoConnection(btn_login, login, senha)

		#login successful
		btn_login, login, senha = self.getContext();
		self.loginSuccessful(btn_login, login, senha)

	def setup(self):
		desiredCaps = desired_capabilities_pan.get_desired_capabilities('pan.apk')
		self.driver = webdriver.Remote('http://localhost:' + str(self.port) + '/wd/hub', desiredCaps)
		self.action = TouchAction(self.driver)
		sleep(SLEEPY_TIME)

	def loginIncompleteArgs(self, btn_login, login, senha):	
		#### no arguments at all ####
		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		print 'Teste sem preencher login e senha...' + self.spec
		if(text_dialog.text == 'Por favor, informe seu e-mail e senha.'):
			print 'OK!'
		else:
			print 'Not OK!'

		###### no passwrod ####

		login.send_keys("email@gmail.com")
		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))

		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		print 'Teste sem preencher senha... ' + self.spec
		if(text_dialog.text == 'Por favor, informe seu e-mail e senha.'):
			print 'OK!'
		else:
			print 'Not OK!'

		#### no login ####

		login.clear()
		senha_txt = "1"
		senha.send_keys(senha_txt)
		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		print 'Teste sem preencher login... ' + self.spec 
		if(text_dialog.text == 'Por favor, informe seu e-mail e senha.'):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

	def loginWrongArgs(self, btn_login, login, senha):
		#pwd letter
		senha_txt = "a"
		senha.send_keys(senha_txt)

		print 'Teste com senha de valor não numerico... ' + self.spec
		if(senha.text == ''):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

		#both invalid
		login.clear()

		login.send_keys("a")
		senha_txt = "1"
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		message = 'Não foi possível validar seu email ou senha. Por favor, verifique os valores e tente novamente.'
		print 'Teste com login e senha errados... ' + self.spec
		if(text_dialog.text == message.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

		#invalid email, valid password
		login.clear()

		login.send_keys("a")
		senha_txt = "123456"
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		message = 'Não foi possível validar seu email ou senha. Por favor, verifique os valores e tente novamente.'
		print 'Teste com login errado e senha certa(dentro do padrão de 6 a 8 digitos)... ' + self.spec
		if(text_dialog.text == message.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)
		
		#valid email, invalid password
		login.clear()
		
		login.send_keys("email@gmail.com")
		senha_txt = "1"
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		message = 'Não foi possível validar seu email ou senha. Por favor, verifique os valores e tente novamente.'
		print 'Teste com login correto(formato de e-mail), porém não cadastrado e senha errada... ' + self.spec
		if(text_dialog.text == message.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

	def loginThreeTimesWrong(self, btn_login, login, senha):
		login.clear()
		self.action.long_press(senha).perform
		senha.clear()
		
		for i in range(0, 2):
			login.send_keys("l.fvm23@gmail.com")
			senha.send_keys("a")

			self.action.tap(btn_login).perform()
			sleep(SLEEPY_TIME)
			btn_ok_dialog = self.driver.find_element_by_id(package + 'button1')
			self.action.tap(btn_ok_dialog).perform()
			sleep(SLEEPY_TIME)

			login.clear()
			self.action.long_press(senha).perform
			senha.clear()

		login.send_keys("l.fvm23@gmail.com")
		senha.send_keys("a")

		self.action.tap(btn_login).perform()
		sleep(SLEEPY_TIME)

		text_dialog = self.driver.find_element_by_id(package + 'message')
		message = 'Seu acesso foi bloqueado. Caso não se lembre de sua senha, por favor, clique em \'Esqueci minha senha\'.'
		print 'Teste com login valido 3x errado... ' + self.spec
		if(text_dialog.text == message.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		btn_ok_dialog = self.driver.find_element_by_id(package + 'button1')
		self.action.tap(btn_ok_dialog).perform()
		sleep(SLEEPY_TIME)

	def loginNotSignedUp(self, btn_login, login, senha):
		#user not signed up 
		login.clear()
		senha_txt = "123456"

		login.send_keys("email@gmail.com.br")
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		message = 'Não foi possível validar seu email ou senha. Por favor, verifique os valores e tente novamente.'
		print 'Teste com usuário não cadastrado no sistema... ' + self.spec
		if(text_dialog.text == message.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

	def loginNoProducts(self, btn_login, login, senha):
		#user has no products 
		login.clear()

		senha_txt = "123456"
		login.send_keys("l.fvm23@gmail.com")
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))
		
		text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
		message = 'Esta versão do aplicativo PAN permite obter informações sobre cartões de crédito e financiamento de veículos. Não encontramos nenhum destes produtos associados aos seus dados. Caso possua algum destes produtos, por favor, entre em contato com a central de atendimento para obter mais informações.'
		print 'Teste com usuário cadastrado(pendente) sem produtos... ' + self.spec
		if(text_dialog.text == message.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

	def loginNoConnection(self, btn_login, login, senha):
		#device not connected
		login.clear()

		if self.driver.network_connection == 1:
			login.send_keys("l.fvm23@gmail.com")
			senha_txt = "123456"
			senha.send_keys(senha_txt)

			self.action.tap(btn_login).perform()
			WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'snackbar_text')))

			text_dialog = self.driver.find_element_by_id(package + 'snackbar_text')
			message = 'Sua conexão com a internet não está disponível neste momento.'
			print 'Teste sem conexão... ' + self.spec
			if(text_dialog.text == message.decode('utf-8')):
				print 'OK!'
			else:
				print 'Not OK!'

			self.action.long_press(senha).perform
			for i in range(0, len(senha_txt)):
				self.driver.press_keycode(67)
			login.clear()

			#wifi
			self.driver.mobile.set_network_connection(self.driver.mobile.WIFI_NETWORK)

	def loginSuccessful(self, btn_login, login, senha):
		#login successful 
		senha_txt = "102030"
		login.send_keys("pedro.fogolin@gmail.com")
		senha.send_keys(senha_txt)

		self.action.tap(btn_login).perform()
		WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.ID, package + 'toolbar_title')))
		
		toolbar_text = self.driver.find_element_by_id(package + 'toolbar_title')
		text = 'Meus Produtos'
		print 'Teste login com sucesso... ' + self.spec 
		if(toolbar_text.text == text.decode('utf-8')):
			print 'OK!'
		else:
			print 'Not OK!'

		self.action.long_press(senha).perform
		for i in range(0, len(senha_txt)):
			self.driver.press_keycode(67)

if __name__ == '__main__':
	thread1 = loginTests(4723, 'Nexus 6, Android 7.0')
	thread2 = loginTests(4724, 'Nexus 4, Android 6.0')

	thread1.start()
	#thread2.start()