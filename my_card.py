#coding=utf-8
#files
from helperPan import *

class myCardTests(threading.Thread):
	def __init__(self, id, port, driver, action):
		threading.Thread.__init__(self)
		self.port = port
		self.action = action
		self.driver = driver
		self.numero_cartao = 'Final ' + id

	def run(self):
		if(self.numero_cartao == 'Final 4402'):
			#in this case, we should click in the alert
			helperPan.dismissBlockedCardAlert()
		self.accessMyCard()
		self.myCard()

	def accessMyCard(self):
		print 'Access My Card...'
		try:
			servicesLabel = self.driver.find_element_by_android_uiautomator('text(\"' + 'Serviços'.decode("utf8") + '\")')
			self.action.tap(servicesLabel).perform()
			print bcolors.OKGREEN + 'OK!' + bcolors.ENDC

			if(self.numero_cartao == 'Final 4402'):
				helperPan.dismissBlockedCardAlert()
		except:
			print bcolors.FAIL + 'Not OK!' + bcolors.ENDC

	def checkCreditLimit(self):
		try:
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'available_credit_limit_value')))
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'available_credit_limit_title')))
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'total_credit_limit_title')))
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'total_credit_limit_value')))
		except TimeoutException as e:
			print 'Algo faltando na tela de meu limite de crédito...'
			print e

		self.driver.back()

	def checkStaticElementsMyCard(self):
		consignado = False
		if(self.numero_cartao == 'Final 4402'):
			consignado = False
		else:
			consignado = True

		currentDebtsTitleZeplin = 'Lançamentos Atuais (Fechado)'
		dueDateZeplin = 'Vencimento'
		queryEntriesZeplin = 'Consulta de Lançamentos'
		creditLimitZeplin = 'Limites de Crédito'
		partialServiceZeplin = 'Parcelamento'

		if(consignado == True):
			duePaymentZeplin = 'Valor total'
			minimumPaymentZeplin = 'Desc. em folha'
		else:
			duePaymentZeplin = 'Total a pagar'
			minimumPaymentZeplin = 'Pagto. mínimo'

		print 'Static elements My Card... '
		if(self.numero_cartao == 'Final 8849'):
			try:
				invoiceLabel = 'Você não possui faturas para este cartão'
				WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'lbl_credit_limit')))
				invoiceLabel = self.driver.find_element_by_android_uiautomator('text(\"' + invoiceLabel.decode("utf8") + '\")')
				creditLimit = self.driver.find_element_by_id(package + 'lbl_credit_limit')
				print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
			except:
				print bcolors.FAIL + 'Not OK!' + bcolors.ENDC
		else:
			WebDriverWait(self.driver, 3).until(EC.presence_of_element_located((By.ID, package + 'current_debts_title')))
			currentDebtsTitle = self.driver.find_element_by_id(package + 'current_debts_title')
			duePayment = self.driver.find_element_by_id(package + 'total_to_pay_title')
			dueDate = self.driver.find_element_by_id(package + 'due_date_title')
			minimumPayment = self.driver.find_element_by_id(package + 'min_to_pay_title')
			queryEntries = self.driver.find_element_by_id(package + 'lbl_debts_consulting')
			creditLimit = self.driver.find_element_by_id(package + 'lbl_credit_limit')

			if(self.numero_cartao == 'Final 4402'):
				if(currentDebtsTitle.text == currentDebtsTitleZeplin.decode('utf8')
					and dueDate.text == dueDateZeplin.decode('utf8')
					and duePayment.text == duePaymentZeplin.decode('utf8')
					and minimumPayment.text == minimumPaymentZeplin.decode('utf8')
					and queryEntries.text == queryEntriesZeplin.decode('utf8')
					and creditLimit.text == creditLimitZeplin.decode('utf8')):
					print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
				else:
					print bcolors.FAIL + 'Not OK!' + bcolors.ENDC
			elif(self.numero_cartao == 'Final 8850'):
				partialService = self.driver.find_element_by_id(package + 'lbl_repayment')
				if(currentDebtsTitle.text == currentDebtsTitleZeplin.decode('utf8')
					and dueDate.text == dueDateZeplin.decode('utf8')
					and duePayment.text == duePaymentZeplin.decode('utf8')
					and minimumPayment.text == minimumPaymentZeplin.decode('utf8')
					and queryEntries.text == queryEntriesZeplin.decode('utf8')
					and creditLimit.text == creditLimitZeplin.decode('utf8')
					and partialService.text == partialServiceZeplin.decode('utf8')):
					print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
				else:
					print bcolors.FAIL + 'Not OK!' + bcolors.ENDC
			elif(self.numero_cartao == 'Final 8852'):
				partialService = self.driver.find_element_by_id(package + 'lbl_repayment')
				if(currentDebtsTitle.text == currentDebtsTitleZeplin.decode('utf8')
					and dueDate.text == dueDateZeplin.decode('utf8')
					and duePayment.text == duePaymentZeplin.decode('utf8')
					and minimumPayment.text == minimumPaymentZeplin.decode('utf8')
					and queryEntries.text == queryEntriesZeplin.decode('utf8')
					and creditLimit.text == creditLimitZeplin.decode('utf8')
					and partialService.text == partialServiceZeplin.decode('utf8')):
					print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
				else:
					print bcolors.FAIL + 'Not OK!' + bcolors.ENDC
	
	def myCard(self):
		self.checkStaticElementsMyCard()
		creditLimitText = 'Limites de Crédito'
		creditLimitLabel = self.driver.find_element_by_android_uiautomator('text(\"' + creditLimitText.decode("utf8") + '\")')
		self.action.tap(creditLimitLabel).perform()
		self.checkCreditLimit()

		if(self.numero_cartao == 'Final 8849'):
			pass
		else:
			print 'Dynamic elements My Card...'
			dueDateValue = self.driver.find_element_by_id(package + 'due_date_value')
			duePayment = self.driver.find_element_by_id(package + 'total_to_pay_value')
			minimumPayment = self.driver.find_element_by_id(package + 'min_to_pay_value')
			showCodeLabel = "Exibir Código"
			
			if(self.numero_cartao == 'Final 4402'):
				cardLastDigitsLabel = self.driver.find_element_by_android_uiautomator('text(\"Final:\")')
				cardLastDigitsNumber = self.driver.find_element_by_android_uiautomator('text(\"4402\")')
				holderNameLabel = self.driver.find_element_by_android_uiautomator('text(\"Titular:\")')
				holderNameNumber = self.driver.find_element_by_android_uiautomator('text(\"Thiago Borges\")')
				showCode = self.driver.find_element_by_android_uiautomator('text(\"' + showCodeLabel.decode("utf8") +  '\")')
				helperPan.checkShowCodeButton(showCode)

				if(dueDateValue.text == '05/06/2014' and
					duePayment.text == 'R$ 1.500,00' and
					minimumPayment.text == 'R$ 123,43'):
					print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
				else:
					print bcolors.FAIL + 'Not OK!' + bcolors.ENDC
			if(self.numero_cartao == 'Final 8850'):
				cardLastDigitsLabel = self.driver.find_element_by_android_uiautomator('text(\"Final:\")')
				cardLastDigitsNumber = self.driver.find_element_by_android_uiautomator('text(\"8850\")')
				holderNameLabel = self.driver.find_element_by_android_uiautomator('text(\"Titular:\")')
				holderNameNumber = self.driver.find_element_by_android_uiautomator('text(\"Daniele Boscolo\")')
				if(dueDateValue.text == '05/06/2014' and
					duePayment.text == 'R$ 1.500,00' and
					minimumPayment.text == 'R$ 123,43'):
					print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
				else:
					print bcolors.FAIL + 'Not OK!' + bcolors.ENDC
			if(self.numero_cartao == 'Final 8852'):
				cardLastDigitsLabel = self.driver.find_element_by_android_uiautomator('text(\"Final:\")')
				cardLastDigitsNumber = self.driver.find_element_by_android_uiautomator('text(\"8852\")')
				holderNameLabel = self.driver.find_element_by_android_uiautomator('text(\"Titular:\")')
				holderNameNumber = self.driver.find_element_by_android_uiautomator('text(\"Jefferson Lima\")')
				showCode = self.driver.find_element_by_android_uiautomator('text(\"' + showCodeLabel.decode("utf8") +  '\")')
				helperPan.checkShowCodeButton(showCode)

				if(dueDateValue.text == '05/06/2014' and
					duePayment.text == 'R$ 1.500,00' and
					minimumPayment.text == 'R$ 123,43'):
					print bcolors.OKGREEN + 'OK!' + bcolors.ENDC
				else:
					print bcolors.FAIL + 'Not OK!' + bcolors.ENDC

if __name__ == '__main__':
	if len(sys.argv) > 2:
		cartoes = ['4402', '8849', '8850', '8852']
		port = 4723
		file = 'pan.apk'
		product = 'cartao'
		helperPan = helperPan()

		for id in cartoes:
				print 'Cartão Final ' + id
				helperPan.initPan(id, port, file, product)
				thread1 = myCardTests(id, port, helperPan.driver, helperPan.action)

				thread1.start()
				while thread1.isAlive():
					pass

				print ' '
	else:
		print 'To run this script you should input if it is android or iphone.'